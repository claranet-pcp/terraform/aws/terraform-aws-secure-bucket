resource "aws_s3_bucket" "bucket" {
  provider = aws
  bucket   = var.name
  region   = var.region
  acl      = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  provider = aws
  bucket   = aws_s3_bucket.bucket.id
  policy   = data.aws_iam_policy_document.bucket_policy.json
}
