require 'rspec'

RSpec.describe $terraform_output do
  $bucket_name = $terraform_output["bucket_name"]["value"]

  describe "#bucket-outputs" do
    it "should have bucket_name" do
      expect($terraform_output["bucket_name"]["value"]).not_to eq("")
      $bucket_name = $terraform_output["bucket_name"]["value"]
      expect($bucket_name).to be_a(String)
    end

    it "should have bucket_region" do
      expect($terraform_output["bucket_region"]["value"]).not_to eq("")
      expect($terraform_output["bucket_region"]["value"]).to be_a(String)
      ENV["AWS_DEFAULT_REGION"] = $terraform_output["bucket_region"]["value"]
    end
  end

  describe "#objects-rw" do
    user_hash_key = "objects-rw-user"
    access_key_id = $terraform_output[user_hash_key + "-access-key"]["value"]
    secret_key = $terraform_output[user_hash_key + "-secret-key"]["value"]

    it "should have a access key" do
      expect(access_key_id).not_to eq("")
      expect(access_key_id).to be_a(String)
    end

    it "should have a secret key" do
      expect(secret_key).not_to eq("")
      expect(secret_key).to be_a(String)
    end

    it "can read bucket attributes" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api get-bucket-acl --bucket " + $bucket_name)).to eq(true)
    end

    it "cannot write to bucket attributes" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api put-bucket-acl --bucket " + $bucket_name + " --acl public-read-write")).not_to eq(true)
    end

    it "can write to bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "echo $AWS_ACCESS_KEY_ID && echo $AWS_SECRET_ACCESS_KEY && aws s3 cp terraform/test.tf s3://" + $bucket_name + "/")).to eq(true)
    end

    it "can list bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 ls s3://" + $bucket_name + "/")).to eq(true)
    end

    it "can read bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp s3://" + $bucket_name + "/test.tf -")).to eq(true)
    end

    it "can delete bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp terraform/test.tf s3://" + $bucket_name + "/test2.tf")).to eq(true)
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 rm s3://" + $bucket_name + "/test2.tf")).to eq(true)
    end
  end

  describe "#readonly-bucket" do
    user_hash_key = "ro-user"

    it "should have a access key" do
      expect($terraform_output[user_hash_key + "-access-key"]["value"]).not_to eq("")
    end

    it "should have a secret key" do
      expect($terraform_output[user_hash_key + "-secret-key"]["value"]).not_to eq("")
    end

    access_key_id = $terraform_output[user_hash_key + "-access-key"]["value"]
    secret_key = $terraform_output[user_hash_key + "-secret-key"]["value"]

    it "can read bucket attributes" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api get-bucket-acl --bucket " + $bucket_name)).to eq(true)
    end

    it "cannot write to bucket attributes" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api put-bucket-acl --bucket " + $bucket_name + " --acl public-read-write")).to_not eq(true)
    end

    it "cannot list bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 ls s3://" + $bucket_name + "/")).not_to eq(true)
    end

    it "cannot write bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp terraform/test.tf s3://" + $bucket_name + "/")).not_to eq(true)
    end

    it "cannot read bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp s3://" + $bucket_name + "/test.tf -")).not_to eq(true)
    end

    it "delete bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 rm s3://" + $bucket_name + "/test.tf")).not_to eq(true)
    end
  end

  describe "#readwrite-bucket" do
    user_hash_key = "rw-user"
    it "should have a access key" do
      expect($terraform_output[user_hash_key + "-access-key"]["value"]).not_to eq("")
    end

    it "should have a secret key" do
      expect($terraform_output[user_hash_key + "-secret-key"]["value"]).not_to eq("")
    end

    access_key_id = $terraform_output[user_hash_key + "-access-key"]["value"]
    secret_key = $terraform_output[user_hash_key + "-secret-key"]["value"]

    it "can write to bucket attributes" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api put-bucket-acl --bucket " + $bucket_name + " --acl public-read-write")).to eq(true)
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api put-bucket-acl --bucket " + $bucket_name + " --acl private")).to eq(true)
    end

    it "can read bucket attributes" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api get-bucket-acl --bucket " + $bucket_name)).to eq(true)
    end

    it "cannot list bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 ls s3://" + $bucket_name + "/")).not_to eq(true)
    end

    it "cannot read bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp s3://" + $bucket_name + "/test.tf -")).not_to eq(true)
    end

    it "cannot write bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp terraform/test.tf s3://" + $bucket_name + "/")).not_to eq(true)
    end

    it "delete bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 rm s3://" + $bucket_name + "/test.tf")).not_to eq(true)
    end
  end

  describe "#other-user" do
    user_hash_key = "other-user"
    it "should have a access key" do
      expect($terraform_output[user_hash_key + "-access-key"]["value"]).not_to eq("")
    end

    it "should have a secret key" do
      expect($terraform_output[user_hash_key + "-secret-key"]["value"]).not_to eq("")
    end

    access_key_id = $terraform_output[user_hash_key + "-access-key"]["value"]
    secret_key = $terraform_output[user_hash_key + "-secret-key"]["value"]

    it "cannot read bucket attributes" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api get-bucket-acl --bucket " + $bucket_name)).not_to eq(true)
    end

    it "cannot write to bucket attributes" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3api put-bucket-acl --bucket " + $bucket_name + " --acl public-read-write")).not_to eq(true)
    end

    it "cannot list bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 ls s3://" + $bucket_name + "/")).not_to eq(true)
    end

    it "cannot write to bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp test.tf s3://" + $bucket_name + "/")).not_to eq(true)
    end

    it "cannot read bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp s3://" + $bucket_name + "/test.tf -")).not_to eq(true)
    end

    it "cannot delete bucket objects" do
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 cp terraform/test.tf s3://" + $bucket_name + "/test2.tf")).not_to eq(true)
      expect(system({"AWS_ACCESS_KEY_ID" => access_key_id, "AWS_SECRET_ACCESS_KEY" => secret_key}, "aws s3 rm s3://" + $bucket_name + "/test2.tf")).not_to eq(true)
    end
  end
end
