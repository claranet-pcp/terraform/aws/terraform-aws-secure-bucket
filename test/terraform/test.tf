terraform {
  backend "local" {
    path = "terraform.tfstate"
  }
}

data "aws_caller_identity" "current" {}

# Test user to ensure that a user that is granted read only access to the bucket can
# only read the bucket's attributes.
resource "aws_iam_user" "readonly-bucket" {
  name = "secure-bucket-test-bucket-ro"
}

resource "aws_iam_access_key" "readonly-bucket" {
  user = aws_iam_user.readonly-bucket.name
}

# Test users to ensure that a user that is granted access to modify the bucket is able
# to modify the main all attributes of the bucket.
resource "aws_iam_user" "readwrite-bucket" {
  name = "secure-bucket-test-bucket-rw"
}

resource "aws_iam_access_key" "readwrite-bucket" {
  user = aws_iam_user.readwrite-bucket.name
}

# Test user to ensure that users that are granted access to the objects within
# the bucket are able to access the objects.
resource "aws_iam_user" "objects-rw" {
  name = "secure-bucket-test-objects-rw"
}

resource "aws_iam_access_key" "objects-rw" {
  user = aws_iam_user.objects-rw.name
}

# Test user to see if users that are granted full access to S3 are able to
# access the bucket in any way.
resource "aws_iam_user" "other-user" {
  name = "secure-bucket-test-other-user"
}

resource "aws_iam_access_key" "other-user" {
  user = aws_iam_user.other-user.name
}

# Test user to see if users that are granted full access to S3 are able to
# access the bucket in any way.
resource "aws_iam_user_policy_attachment" "ro-user-s3-full-access" {
  user       = aws_iam_user.readonly-bucket.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_user_policy_attachment" "rw-user-s3-full-access" {
  user       = aws_iam_user.readwrite-bucket.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_user_policy_attachment" "rw-object-user-s3-full-access" {
  user       = aws_iam_user.objects-rw.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_user_policy_attachment" "other-user-s3-full-access" {
  user       = aws_iam_user.other-user.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

module "securebucket" {
  source = "../../"
  name   = "claranet-test-mysecurebucket"
  region = "eu-west-2"

  bucket_read_only_uids = [
    aws_iam_user.readonly-bucket.unique_id,
    data.aws_caller_identity.current.account_id,
    data.aws_caller_identity.current.user_id,
  ]

  bucket_read_write_uids = [
    aws_iam_user.readwrite-bucket.unique_id,
    data.aws_caller_identity.current.account_id,
    data.aws_caller_identity.current.user_id,
  ]

  bucket_read_write_objects_uids = [
    aws_iam_user.objects-rw.unique_id,
    data.aws_caller_identity.current.account_id,
    data.aws_caller_identity.current.user_id,
  ]
}

output "ro-user-access-key" {
  value = aws_iam_access_key.readonly-bucket.id
}

output "ro-user-secret-key" {
  value = aws_iam_access_key.readonly-bucket.secret
}

output "rw-user-access-key" {
  value = aws_iam_access_key.readwrite-bucket.id
}

output "rw-user-secret-key" {
  value = aws_iam_access_key.readwrite-bucket.secret
}

output "objects-rw-user-access-key" {
  value = aws_iam_access_key.objects-rw.id
}

output "objects-rw-user-secret-key" {
  value = aws_iam_access_key.objects-rw.secret
}

output "other-user-access-key" {
  value = aws_iam_access_key.other-user.id
}

output "other-user-secret-key" {
  value = aws_iam_access_key.other-user.secret
}

output "bucket_region" {
  value = module.securebucket.bucket_region
}

output "bucket_name" {
  value = module.securebucket.bucket_name
}
