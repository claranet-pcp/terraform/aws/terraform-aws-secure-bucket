require 'json'

ENV['AWS_PROFILE'] = ENV['AWS_DEFAULT_PROFILE']

Dir.chdir('terraform') {
	system('terraform init') or raise 'Terraform init failed'
	system('terraform plan') or raise 'Terraform plan failed'
	system('terraform get') or raise 'Terraform get failed'
	system('terraform apply') or raise 'Terraform apply failed'
	$terraform_output = JSON.parse(`terraform output --json`)
}