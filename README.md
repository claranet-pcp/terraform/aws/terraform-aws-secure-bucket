# terraform-aws-secure-bucket

This Terraform module creates a secure S3 bucket that denies access to all users (even those with S3FullAccess) unless they
are white listed. The bucket is also encrypted using S3-SSE by default.

*Important note* the module currently only white lists the ability for UIDs to have the ability to access the bucket if already granted access by another IAM policy
so if you white list a UID that does not already have a policy granting access to the bucket they will still be denied access. If you want to white lists a Role ID you
must also ensure that you white list the role session name or do a wild card on session names on the role.  

To elaborate the ID format the aws:userid sees is:

```
UNIQUE-ROLE-ID:ROLE-SESSION-NAME
```

so if your Role ID is `AROA00000000000000000` when assumed it will be appended with a session name like `AROA00000000000000000:mysession` so either white list `AROA00000000000000000:mysession` and
any other session names you will want to access the bucket with or do a wild card match `AROA00000000000000000:*`.

# Terraform Version Compatibility
| Module version | Terraform version |
|----------------|-------------------|
| 2.x.x          | 0.12.x            |
| 1.x.x          | 0.11.x            |

## Features

* Creates a S3 bucket with a restrictive IAM policy that blocks *all* access to the bucket to everyone unless their user id, role id or account number is listed.
* Sets the bucket to be encrypted using S3 server side encryption using the default S3 KMS key by default.

## Usage

```js
module "secure-bucket" {
  source = "localmodules/securebucket"
  name   = "mysecurebucket"
  region = "eu-west-2"

  bucket_read_only_uids = [
    "AROA00000000000000000", # role id example
    "AROA00000000000000001", # 2nd role id example
    "111111111111", # account number example
  ]

  bucket_read_write_uids = [
    "AROA00000000000000001", # 2nd role id example
    "111111111111", # account number example
  ]

  bucket_read_write_objects_uids = [
    "AIDA00000000000000000", # user id example
    "111111111111", # account number example
  ]
}
```
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| bucket_read_only_uids | A list of AWS UIDs (User IDs, Role IDs or account numbers) not to block access to read bucket attributes. | list | `<list>` | no |
| bucket_read_write_objects_uids | A list of AWS UIDs (User IDs, Role IDs or account numbers) not to block access to read and write to bucket objects. | list | `<list>` | no |
| bucket_read_write_uids | A list of AWS UIDs (User IDs, Role IDs or account numbers) not to block access to write to bucket attributes. | list | `<list>` | no |
| name | The name of the bucket. | string | - | yes |
| region | The region to create the bucket. | string | - | yes |

## Running tests

The bucket comes with tests using rspec.

To run these execute:
```
rspec tests_spec.rb
```

from the test directory.
