variable "name" {
  description = "The name of the bucket."
}

variable "region" {
  description = "The region to create the bucket."
}

variable "bucket_read_only_uids" {
  type        = list(string)
  description = "A list of user, role or account ids to white list for read only access to the bucket's attributes."
  default     = []
}

variable "bucket_read_write_uids" {
  type        = list(string)
  description = "A list of user, role or account ids to white list for write access to the bucket's attributes."
  default     = []
}

variable "bucket_read_write_objects_uids" {
  type        = list(string)
  description = "A list of user, role or account ids to white list for read write access to the bucket's objects."
  default     = []
}
